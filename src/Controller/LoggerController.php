<?php

namespace App\Controller;

use App\Service\RandomSlogan;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LoggerController extends AbstractController
{
    /**
     * @Route("/logger", name="logger")
     */
    public function index(RandomSlogan $randomSlogan)
    {
        return $this->render('logger/index.html.twig', [
            'controller_name' => 'LoggerController',
            'roxy' => $randomSlogan ->getHappyMessage()
        ]);
    }
}
