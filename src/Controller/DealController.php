<?php

namespace App\Controller;

use App\Entity\Deal;
use App\Form\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DealController extends AbstractController
{
    /**
     * @Route({"list" : "/deal/list", "index" : "/"}, name="deal_list", methods={"GET","HEAD"})
     *
     */
    public function index()
    {
        return new Response("hello",200);
    }

    /**
     * @Route("/deal/show/{index}", name="deal_show", methods={"GET","HEAD"}, requirements = {"index"="\d+"})
     *
     */ //d+ signifie que c'esst seulement des entiers qui peuvent etre passés
    public function show($index)
    {
        return new Response($index, 200);
    }

    /**
     * @Route("/deal/showForm", name="deal_showForm")
     *
     */
    public function showForm(Request $request)
    {
        $deal = new Deal();
        $form = $this->createForm(FormType::class, $deal);
        return $this->render('deal/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
