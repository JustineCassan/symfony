<?php

namespace App\Controller;

use App\Entity\Apprendre;
use App\Form\FormApprendreType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApprendreController extends AbstractController
{
    /**
     * @Route("/apprendre", name="apprendre")
     */
    public function index(Request $request)//request import httpfoundation
    {
        $apprendre = new Apprendre();
        $form = $this->createForm(FormApprendreType::class, $apprendre);
        return $this->render('apprendre/index.html.twig', [//désigne la page (tjrs page twig) ou on va être envoyé pour le rendu
            'controller_name' => 'ApprendreController',//controller_name c'est le nom que l'on donne pour l'appel et après la flèche c'est ce qui s'affiche sur la page lorsque il est appelé dans twig
            'formulaire' => $form->createView(),
        ]);
    }
}
