<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class RandomSlogan{

    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getHappyMessage()
    {
        $messages = [
            'I\'m good enough, I\'m smart enough, and doggonit, people like me!',
            'I deserve good things.',
            'I am entitled to my share of happiness.',
            'I refuse to beat myself up.',
            'I\'m going to do a terrific show today!',
            'That\'s just stinkin\' thinkin!',
            'I am a human being, not a human doing.',
            'Trace it, face it, and erase it.',
            'Denial ain\'t just a river in Egypt!',
            'Compare and despair.',
        ];

        $index = array_rand($messages);

        $this->logger->info($messages[$index]);
        return $messages[$index];
    }
}