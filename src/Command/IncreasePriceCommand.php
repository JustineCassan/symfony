<?php

namespace App\Command;

use App\Entity\Deal;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class PriceIncreaseCommand extends Command
{
    protected static $defaultName = 'PriceIncreaseCommand';

    protected function configure()
    {
        $this
            ->setName('deal:price:increase')
            ->setDescription('Increase the price')
            ->addArgument('price', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('id', xxxx, InputOption::OPTIONAL, 'Option description')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Option description')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('id') !== null && $input->getOption('all') !== null) {
            // ...
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }
}
