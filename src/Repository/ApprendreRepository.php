<?php

namespace App\Repository;

use App\Entity\Apprendre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Apprendre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apprendre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apprendre[]    findAll()
 * @method Apprendre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApprendreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Apprendre::class);
    }

    // /**
    //  * @return Apprendre[] Returns an array of Apprendre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Apprendre
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
